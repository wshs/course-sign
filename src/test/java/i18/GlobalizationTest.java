package i18;

import java.util.Locale;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import junit.AbstractTest;

public class GlobalizationTest extends AbstractTest {

	@Autowired
	MessageSource messageSource;

	@Test
	public void testResourceBundleMessageSource() {
		String username_us = messageSource.getMessage("test.name.pattern.error", new Object[1], Locale.US);
		String username_chinese = messageSource.getMessage("test.name.pattern.error", new Object[0], Locale.CHINESE);
		System.out.println("chinese:" + username_chinese);
		System.out.println("english:" + username_us);
	}
}
