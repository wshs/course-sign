package ServiceTest;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import junit.AbstractTest;
import signHelper.modle.Institution;
import signHelper.service.InstitutionService;

public class InstitutionTest extends AbstractTest{
	@Autowired InstitutionService service;
	
	@Test
	@Ignore
	public void test(){
		Institution institution=new Institution();
		institution.setName("信息科学与工程学院1");
		institution.setSchoolId(2);
		institution.setUrl("http://www.hnu.edu.cn/1");
		
		service.add(institution);
		
		institution.setName("信息科学与工程学院");
		institution.setUrl("http://www.hnu.edu.cn/");
		
		service.update(institution);
		
		System.out.println(service.query(new Institution(), 0, 5));
		
		System.out.println(service.getById(service.query(new Institution(), 0, 5).get(0).getId()));
		
		service.delete(service.query(new Institution(), 0, 5).get(0).getId());
		
		service.add(institution);
	}
	
	
}
