package ServiceTest;

import java.util.Date;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import junit.AbstractTest;
import signHelper.modle.Sign;
import signHelper.service.SignService;

public class SignTest extends AbstractTest{
	@Autowired SignService service;
	
	@Test
	//@Ignore
	public void test(){
		Sign sign=new Sign();
		sign.setCourseRecordId(2);
		sign.setStuId(3);
		sign.setType(1);
		sign.setSignIn(new Date());
		sign.setSignOut(new Date());
		
		service.add(sign);
	
		sign.setCourseRecordId(3);
		sign.setStuId(4);
		sign.setType(2);
		sign.setSignIn(new Date());
		sign.setSignOut(new Date());
		
		service.update(sign);
		
		System.out.println(service.query(new Sign(), 0, 5));
		
		System.out.println(service.getById(service.query(new Sign(), 0, 5).get(0).getId()));
		
		service.delete(service.query(new Sign(), 0, 5).get(0).getId());
		
	}
	
	
}
