package ServiceTest;

import java.util.Date;

import org.apache.shiro.crypto.hash.Md5Hash;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import junit.AbstractTest;
import signHelper.modle.User;
import signHelper.service.UserService;

public class UserTest extends AbstractTest{
	@Autowired UserService service;
	
	@Test
	@Ignore
	public void test(){
		User user=new User();
		user.setClassId(2);
		user.setDeviceModifyDate(new Date());
		user.setDeviceNumber("XYYFU-HKJFF-SYFHN-HFSIY");
		user.setEmail("1265976@qq.com");
		user.setInstitutionId(2);
		user.setNumber("201326010302");
		user.setName("王焕");
		user.setPassword(new Md5Hash("321321").toString());
		user.setRole(1);
		user.setSex(1);
		
		service.add(user);
		
		user.setClassId(2);
		user.setDeviceModifyDate(new Date());
		user.setDeviceNumber("XYYFU-HKJFF-SYFHN-HFSIY1111");
		user.setEmail("1265976@qq.com111");
		user.setInstitutionId(2);
		user.setNumber("2013260103021111");
		user.setName("王焕11");
		user.setPassword(new Md5Hash("32132111").toString());
		user.setRole(2);
		user.setSex(1);
		
		service.update(user);
		
		System.out.println(service.query(new User(), 0, 5));
		
		System.out.println(service.getById(service.query(new User(), 0, 5).get(0).getId()));
		
		service.delete(service.query(new User(), 0, 5).get(0).getId());
	}
	
	
}
