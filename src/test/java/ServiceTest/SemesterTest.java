package ServiceTest;

import java.time.LocalDate;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import junit.AbstractTest;
import signHelper.modle.Semester;
import signHelper.service.SemesterService;

public class SemesterTest extends AbstractTest{
	@Autowired SemesterService service;
	
	@Test
	@Ignore
	public void test(){
		Semester semester=new Semester();
		semester.setName("2014-2015 秋");
		semester.setSchoolId(3);
		semester.setStartDate(LocalDate.parse("2017-11-11"));
		semester.setEndDate(LocalDate.parse("2017-11-30"));
		service.add(semester);
		
		semester.setName("2014-2015 春");
		semester.setSchoolId(2);
		semester.setStartDate(LocalDate.parse("2017-02-11"));
		semester.setEndDate(LocalDate.parse("2017-03-30"));
		
		service.update(semester);
		
		System.out.println(service.query(new Semester(), 0, 5));
		
		System.out.println(service.getById(service.query(new Semester(), 0, 5).get(0).getId()));
		
		service.delete(service.query(new Semester(), 0, 5).get(0).getId());
		
		service.add(semester);
	}
	
	
}
