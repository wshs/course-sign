package ServiceTest;

import java.util.Date;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import junit.AbstractTest;
import signHelper.modle.CourseRecord;
import signHelper.service.CourseRecordService;

public class CourseRecordTest extends AbstractTest{
	@Autowired CourseRecordService service;
	
	@Test
	@Ignore
	public void test(){
		CourseRecord courseRecord=new CourseRecord();
		courseRecord.setCourseId(5);
		courseRecord.setStartTime(new Date());
		courseRecord.setEndTime(new Date());
		service.add(courseRecord);
		
		courseRecord.setCourseId(6);
		courseRecord.setStartTime(new Date());
		courseRecord.setEndTime(new Date());
		
		service.update(courseRecord);
		
		System.out.println(service.query(new CourseRecord(), 0, 5));
		
		System.out.println(service.getById(service.query(new CourseRecord(), 0, 5).get(0).getId()));
		
		service.delete(service.query(new CourseRecord(), 0, 5).get(0).getId());
	}
	
	
}
