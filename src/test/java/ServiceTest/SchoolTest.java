package ServiceTest;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import junit.AbstractTest;
import signHelper.modle.School;
import signHelper.service.SchoolService;

public class SchoolTest extends AbstractTest{
	@Autowired SchoolService service;
	
	@Test
	@Ignore
	public void test(){
		School school=new School();
		school.setName("湖南大学1");
		school.setLocation("湖南省长沙市岳麓区湖南大学1");
		school.setCode("hnu1");
		school.setUrl("http://www.hnu.edu.cn/1");
		
		service.add(school);
		
		school.setName("湖南大学");
		school.setLocation("湖南省长沙市岳麓区湖南大学");
		school.setCode("hnu");
		school.setUrl("http://www.hnu.edu.cn/");
		
		service.update(school);
		
		System.out.println(service.query(new School(), 0, 5));
		
		System.out.println(service.getById(service.query(new School(), 0, 5).get(0).getId()));
		
		service.delete(service.query(new School(), 0, 5).get(0).getId());
	}
	
	
}
