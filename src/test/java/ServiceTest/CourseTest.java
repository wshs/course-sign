package ServiceTest;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import junit.AbstractTest;
import signHelper.modle.Course;
import signHelper.service.CourseService;

public class CourseTest extends AbstractTest{
	@Autowired CourseService service;
	
	@Test
	//@Ignore
	public void test(){
		Course course=new Course();
		course.setCoordinate("111,111");
		course.setDescription("软件工程dec1");
		course.setEndWeek(7);
		course.setEvenTime("1 11:11:11-11:22:33");
		course.setInstitutionId(2);
		course.setLocation("湖大信科院1");
		course.setName("软件工程1");
		course.setOddTime("2 11:11:11-11:22:33");
		course.setSemesterId(4);
		course.setStartWeek(1);
		course.setTeacherId(3);
		
		service.add(course);
		
		course.setCoordinate("112,112");
		course.setDescription("软件工程dec");
		course.setEndWeek(11);
		course.setEvenTime("1 21:22:11-11:22:33");
		course.setInstitutionId(2);
		course.setLocation("湖大信科院");
		course.setName("软件工程");
		course.setOddTime("2 12:22:11-11:22:33");
		course.setSemesterId(5);
		course.setStartWeek(4);
		course.setTeacherId(3);
		
		service.update(course);
		
		System.out.println(service.query(new Course(), 0, 5));
		
		System.out.println(service.getById(service.query(new Course(), 0, 5).get(0).getId()));
		
		service.delete(service.query(new Course(), 0, 5).get(0).getId());
	}
	
	
}
