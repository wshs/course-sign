package signHelper.dao;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import signHelper.modle.School;

@Repository
public class SchoolDao {
	private static final RowMapper<School> rm = JdbcHelper.getRowMapper(School.class);
	private @Autowired JdbcTemplate jdbcTemplate;
	private @Autowired NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	public School add(School param) {
		param.setId(JdbcHelper.insertAndGetKey(namedParameterJdbcTemplate,Sql.ADD, param));
		return param;
	}
	
	public School getById(Integer id){
		return JdbcHelper.queryForObject(jdbcTemplate, Sql.GET_BY_ID, rm, id);
	}
	
	public List<School> query(School param,Integer has,Integer nextSize) {
		StringBuilder sql = new StringBuilder(Sql.GET);
		if (StringUtils.isNotBlank(param.getName())) {
			sql.append(" and name=:name");
		}
		if (StringUtils.isNotBlank(param.getCode())) {
			sql.append(" and code=:code");
		}
		if(has!=null&&nextSize!=null)
			sql.append(" limit ").append(has).append(",").append(nextSize);
		return namedParameterJdbcTemplate.query(sql.toString(), JdbcHelper.getSqlParameterSource(param), rm);
	}
	
	public void update(School bean) {
		namedParameterJdbcTemplate.update(Sql.UPDATE, JdbcHelper.getSqlParameterSource(bean));
	}
	
	public void delete(Integer id) {
		jdbcTemplate.update(Sql.DEL_BY_ID, id);
	}
	
	private class Sql {
		public static final String GET = "select * from school where 1=1";
		public static final String GET_BY_ID = "select * from school where id=?";
		public static final String ADD = "insert into school (name,code,location,url, create_date,update_date) values (:name,:code,:location,:url,now(),now())";
		public static final String DEL_BY_ID = "delete from school where id=?";
		public static final String UPDATE = "update school set name=ifnull(:name,name),code=ifnull(:code,code),location=ifnull(:location,location),url=ifnull(:url,url) where id=:id";
	}


	
}
