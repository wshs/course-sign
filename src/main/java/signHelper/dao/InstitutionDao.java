package signHelper.dao;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import signHelper.modle.Institution;
import signHelper.modle.School;
import signHelper.modle.User;

@Repository
public class InstitutionDao {
	private final Logger logger = LogManager.getLogger(getClass());
	private static final RowMapper<Institution> rm = JdbcHelper.getRowMapper(Institution.class);
	private @Autowired JdbcTemplate jdbcTemplate;
	private @Autowired NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	public Institution add(Institution param) {
		param.setId(JdbcHelper.insertAndGetKey(namedParameterJdbcTemplate,Sql.ADD, param));
		return param;
	}
	
	public Institution getById(Integer id){
		return JdbcHelper.queryForObject(jdbcTemplate, Sql.GET_BY_ID, rm, id);
	}
	
	public List<Institution> query(Institution param,Integer has,Integer nextSize) {
		StringBuffer sql = new StringBuffer(Sql.GET);
		if (StringUtils.isNotBlank(param.getName())) {
			sql.append(" and name=:name");
		}
		if (param.getSchoolId()!=null) {
			sql.append(" and school_id=:schoolId");
		}
		if(has!=null&&nextSize!=null)
			sql.append(" limit ").append(has).append(",").append(nextSize);
		return namedParameterJdbcTemplate.query(sql.toString(), JdbcHelper.getSqlParameterSource(param), rm);
	}
	
	public void update(Institution bean) {
		namedParameterJdbcTemplate.update(Sql.UPDATE, JdbcHelper.getSqlParameterSource(bean));
	}
	
	public void delete(Integer id) {
		jdbcTemplate.update(Sql.DEL_BY_ID, id);
	}
	
	private class Sql {
		public static final String GET = "select * from institution where 1=1";
		public static final String GET_BY_ID = "select * from institution where id=?";
		public static final String ADD = "insert into institution (name,school_id,url, create_date,update_date) values (:name,:schoolId,:url,now(),now())";
		public static final String DEL_BY_ID = "delete from institution where id=?";
		public static final String UPDATE = "update institution set name=ifnull(:name,name),school_id=ifnull(:schoolId,school_id),url=ifnull(:url,url) where id=:id";
	}


	
}
