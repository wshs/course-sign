package signHelper.dao;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import signHelper.modle.Class;

@Repository
public class ClassDao {
	private final Logger logger = LogManager.getLogger(getClass());
	private static final RowMapper<Class> rm = JdbcHelper.getRowMapper(Class.class);
	private @Autowired JdbcTemplate jdbcTemplate;
	private @Autowired NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	public Class add(Class param) {
		param.setId(JdbcHelper.insertAndGetKey(namedParameterJdbcTemplate,Sql.ADD, param));
		return param;
	}
	
	public Class getById(Integer id){
		return JdbcHelper.queryForObject(jdbcTemplate, Sql.GET_BY_ID, rm, id);
	}
	
	public List<Class> query(Class param,Integer has,Integer nextSize) {
		StringBuffer sql = new StringBuffer(Sql.GET);
		if (StringUtils.isNotBlank(param.getName())) {
			sql.append(" and name=:name");
		}
		if (param.getMajorId()!=null) {
			sql.append(" and major_id=:majorId");
		}
		if(has!=null&&nextSize!=null)
			sql.append(" limit ").append(has).append(",").append(nextSize);
		return namedParameterJdbcTemplate.query(sql.toString(), JdbcHelper.getSqlParameterSource(param), rm);
	}
	
	public void update(Class bean) {
		namedParameterJdbcTemplate.update(Sql.UPDATE, JdbcHelper.getSqlParameterSource(bean));
	}
	
	public void delete(Integer id) {
		jdbcTemplate.update(Sql.DEL_BY_ID, id);
	}
	
	private class Sql {
		public static final String GET = "select * from class where 1=1";
		public static final String GET_BY_ID = "select * from class where id=?";
		public static final String ADD = "insert into class (name,major_id,create_date,update_date) values (:name,:majorId,now(),now())";
		public static final String DEL_BY_ID = "delete from class where id=?";
		public static final String UPDATE = "update class set name=ifnull(:name,name),major_id=ifnull(:majorId,major_id) where id=:id";
	}


	
}
