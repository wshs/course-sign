package signHelper.dao;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import signHelper.modle.User;

@Repository
public class UserDao {
	private final Logger logger = LogManager.getLogger(getClass());
	private static final RowMapper<User> rm = JdbcHelper.getRowMapper(User.class);
	private @Autowired JdbcTemplate jdbcTemplate;
	private @Autowired NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	public User add(User param) {
		param.setId(JdbcHelper.insertAndGetKey(namedParameterJdbcTemplate,Sql.ADD, param));
		return param;
	}
	
	public User getById(Integer id){
		return JdbcHelper.queryForObject(jdbcTemplate, Sql.GET_BY_ID, rm, id);
	}
	
	public List<User> query(User param,Integer has,Integer nextSize) {
		StringBuffer sql = new StringBuffer(Sql.GET);
		if (StringUtils.isNotBlank(param.getName())) {
			sql.append(" and name=:name");
		}
		if (param.getNumber()!=null) {
			sql.append(" and number=:number");
		}
		if (param.getPassword()!=null) {
			sql.append(" and password=:password");
		}
		if (param.getInstitutionId()!=null) {
			sql.append(" and institution_id=:institutionId");
		}
		if (param.getClassId()!=null) {
			sql.append(" and class_id=:classId");
		}
		if (param.getRole()!=null) {
			sql.append(" and role=:role");
		}
		if (param.getSex()!=null) {
			sql.append(" and sex=:sex");
		}
		if (param.getDeviceNumber()!=null) {
			sql.append(" and device_number=:deviceNumber");
		}
		if(has!=null&&nextSize!=null)
			sql.append(" limit ").append(has).append(",").append(nextSize);
		return namedParameterJdbcTemplate.query(sql.toString(), JdbcHelper.getSqlParameterSource(param), rm);
	}
	
	public void update(User bean) {
		namedParameterJdbcTemplate.update(Sql.UPDATE, JdbcHelper.getSqlParameterSource(bean));
	}
	
	public void delete(Integer id) {
		jdbcTemplate.update(Sql.DEL_BY_ID, id);
	}
	
	private class Sql {
		public static final String GET = "select * from user where 1=1";
		public static final String GET_BY_ID = "select * from user where id=?";
		public static final String ADD = "insert into user (number,institution_id,class_id,role,name,password,sex,email,device_number,device_modify_date,create_date,update_date) values (:number,:institutionId,:classId,:role,:name,:password,:sex,:email,:deviceNumber,:deviceModifyDate,now(),now())";
		public static final String DEL_BY_ID = "delete from user where id=?";
		public static final String UPDATE = "update user set number=ifnull(:number,number),institution_id=ifnull(:institutionId,institution_id),class_id=ifnull(:classId,class_id),role=ifnull(:role,role),name=ifnull(:name,name),password=ifnull(:password,password),sex=ifnull(:sex,sex),email=ifnull(:email,email),device_number=ifnull(:deviceNumber,device_number),device_modify_date=ifnull(:deviceModifyDate,device_modify_date) where id=:id";
	}
	
}
