package signHelper.dao;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import signHelper.modle.Course;

@Repository
public class CourseDao {
	private final Logger logger = LogManager.getLogger(getClass());
	private static final RowMapper<Course> rm = JdbcHelper.getRowMapper(Course.class);
	private @Autowired JdbcTemplate jdbcTemplate;
	private @Autowired NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	public Course add(Course param) {
		param.setId(JdbcHelper.insertAndGetKey(namedParameterJdbcTemplate, Sql.ADD, param));
		return param;
	}

	public Course getById(Integer id) {
		return JdbcHelper.queryForObject(jdbcTemplate, Sql.GET_BY_ID, rm, id);
	}

	public List<Course> query(Course param, Integer has, Integer nextSize) {
		StringBuffer sql = new StringBuffer(Sql.GET);
		if (StringUtils.isNotBlank(param.getName())) {
			sql.append(" and name=:name");
		}
		if (param.getSemesterId() != null) {
			sql.append(" and semester_id=:semesterId");
		}
		if (param.getInstitutionId() != null) {
			sql.append(" and institution_id=:institutionId");
		}
		if (param.getTeacherId() != null) {
			sql.append(" and teacher_id=:teacherId");
		}

		if (has != null && nextSize != null)
			sql.append(" limit ").append(has).append(",").append(nextSize);
		return namedParameterJdbcTemplate.query(sql.toString(), JdbcHelper.getSqlParameterSource(param), rm);
	}

	public void update(Course bean) {
		namedParameterJdbcTemplate.update(Sql.UPDATE, JdbcHelper.getSqlParameterSource(bean));
	}

	public void delete(Integer id) {
		jdbcTemplate.update(Sql.DEL_BY_ID, id);
	}

	public void addStuCourseRel(Integer stuId, Integer courseId) {
		jdbcTemplate.update(Sql.ADD_STU_COURSE_REL, stuId, courseId);
	}

	public void delStuCourseRel(Integer stuId, Integer courseId) {
		jdbcTemplate.update(Sql.DEL_STU_COURSE_REL, stuId, courseId);
	}

	private class Sql {
		public static final String GET = "select * from course where 1=1";
		public static final String GET_BY_ID = "select * from course where id=?";
		public static final String ADD = "insert into course (institution_id,teacher_id,name,location,coordinate,description,semester_id,start_week,end_week,odd_time,even_time,create_date,update_date) values (:institutionId,:teacherId,:name,:location,:coordinate,:description,:semesterId,:startWeek,:endWeek,:oddTime,:evenTime,now(),now())";
		public static final String DEL_BY_ID = "delete from course where id=?";
		public static final String UPDATE = "update course set institution_id=ifnull(:institutionId,institution_id),teacher_id=ifnull(:teacherId,teacher_id),name=ifnull(:name,name),location=ifnull(:location,location),coordinate=ifnull(:coordinate,coordinate),description=ifnull(:description,description),semester_id=ifnull(:semesterId,semester_id),start_week=ifnull(:startWeek,start_week),end_week=ifnull(:endWeek,end_week),odd_time=ifnull(:oddTime,odd_time),even_time=ifnull(:evenTime,even_time) where id=:id";
		public static final String ADD_STU_COURSE_REL = "insert into stu_course_rel (stu_id,course_id,create_date,update_date) values (?,?,now(),now())";
		public static final String DEL_STU_COURSE_REL = "delete from stu_course_rel where stu_id=? and course_id=?";
	}

}
