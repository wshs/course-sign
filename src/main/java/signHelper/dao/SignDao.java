package signHelper.dao;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import signHelper.modle.Sign;
@Repository
public class SignDao {
	private final Logger logger = LogManager.getLogger(getClass());
	private static final RowMapper<Sign> rm = JdbcHelper.getRowMapper(Sign.class);
	private @Autowired JdbcTemplate jdbcTemplate;
	private @Autowired NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	public Sign add(Sign param) {
		param.setId(JdbcHelper.insertAndGetKey(namedParameterJdbcTemplate,Sql.ADD, param));
		return param;
	}
	
	public Sign getById(Integer id){
		return JdbcHelper.queryForObject(jdbcTemplate, Sql.GET_BY_ID, rm, id);
	}
	
	public List<Sign> query(Sign param,Integer has,Integer nextSize) {
		StringBuffer sql = new StringBuffer(Sql.GET);
		if (param.getStuId()!=null) {
			sql.append(" and stu_id=:stuId");
		}
		if (param.getCourseRecordId()!=null) {
			sql.append(" and course_record_id=:courseRecordId");
		}
		if(has!=null&&nextSize!=null)
			sql.append(" limit ").append(has).append(",").append(nextSize);
		return namedParameterJdbcTemplate.query(sql.toString(), JdbcHelper.getSqlParameterSource(param), rm);
	}
	
	public void update(Sign bean) {
		namedParameterJdbcTemplate.update(Sql.UPDATE, JdbcHelper.getSqlParameterSource(bean));
	}
	
	public void delete(Integer id) {
		jdbcTemplate.update(Sql.DEL_BY_ID, id);
	}
	
	private class Sql {
		public static final String GET = "select * from sign where 1=1";
		public static final String GET_BY_ID = "select * from sign where id=?";
		public static final String ADD = "insert into sign (stu_id,course_record_id,type,sign_in,sign_out,create_date,update_date) values (:stuId,:courseRecordId,:type,:signIn,:signOut,now(),now())";
		public static final String DEL_BY_ID = "delete from sign where id=?";
		public static final String UPDATE = "update sign set stu_id=ifnull(:stuId,stu_id),course_record_id=ifnull(:courseRecordId,course_record_id),type=ifnull(:type,type),sign_in=ifnull(:signIn,sign_in),sign_out=ifnull(:signOut,sign_out) where id=:id";
	}


	
}
