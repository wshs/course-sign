package signHelper.modle;

public class Course {
	
	private Integer id;  
	private Integer institutionId;
	private Integer teacherId;
    private String name;
    private String location;
    private String coordinate;
	private String description;
    private Integer semesterId;
    private Integer startWeek;
    private Integer endWeek;
    private String oddTime;
    private String evenTime;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getInstitutionId() {
		return institutionId;
	}
	public void setInstitutionId(Integer institutionId) {
		this.institutionId = institutionId;
	}
	public Integer getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(Integer teacherId) {
		this.teacherId = teacherId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getSemesterId() {
		return semesterId;
	}
	public void setSemesterId(Integer semesterId) {
		this.semesterId = semesterId;
	}
	public Integer getStartWeek() {
		return startWeek;
	}
	public void setStartWeek(Integer startWeek) {
		this.startWeek = startWeek;
	}
	public Integer getEndWeek() {
		return endWeek;
	}
	public void setEndWeek(Integer endWeek) {
		this.endWeek = endWeek;
	}
	public String getOddTime() {
		return oddTime;
	}
	public void setOddTime(String oddTime) {
		this.oddTime = oddTime;
	}
	public String getEvenTime() {
		return evenTime;
	}
	public void setEvenTime(String evenTime) {
		this.evenTime = evenTime;
	}
	public String getCoordinate() {
		return coordinate;
	}
	public void setCoordinate(String coordinate) {
		this.coordinate = coordinate;
	}
	@Override
	public String toString() {
		return "Course [id=" + id + ", institutionId=" + institutionId + ", teacherId=" + teacherId + ", name=" + name
				+ ", location=" + location + ", coordinate=" + coordinate + ", description=" + description
				+ ", semesterId=" + semesterId + ", startWeek=" + startWeek + ", endWeek=" + endWeek + ", oddTime="
				+ oddTime + ", evenTime=" + evenTime + "]";
	}
    
}
