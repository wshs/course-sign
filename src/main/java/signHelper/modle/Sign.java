package signHelper.modle;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class Sign {
	
	private Integer id;  
	private Integer stuId;
	private Integer courseRecordId;
    private Integer type;
    private Date signIn;
    private Date signOut;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getStuId() {
		return stuId;
	}
	public void setStuId(Integer stuId) {
		this.stuId = stuId;
	}
	public Integer getCourseRecordId() {
		return courseRecordId;
	}
	public void setCourseRecordId(Integer courseRecordId) {
		this.courseRecordId = courseRecordId;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	public Date getSignIn() {
		return signIn;
	}
	public void setSignIn(Date signIn) {
		this.signIn = signIn;
	}
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	public Date getSignOut() {
		return signOut;
	}
	public void setSignOut(Date signOut) {
		this.signOut = signOut;
	}
	@Override
	public String toString() {
		return "Sign [id=" + id + ", stuId=" + stuId + ", courseRecordId=" + courseRecordId + ", type=" + type
				+ ", signIn=" + signIn + ", signOut=" + signOut + "]";
	}
    
}
