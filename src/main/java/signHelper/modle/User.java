package signHelper.modle;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class User {
	
	private Integer id;  
	private String number;
	private Integer institutionId;
	private Integer classId;
	/**
	 * 0游客 1 老师 2 学生
	 */
	private Integer role;
    private String name;
	private String password;
    private Date deviceModifyDate;
    /**
     * 0男 1女
     */
    private Integer sex;
    private String email;
    private String deviceNumber;
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the number
	 */
	public String getNumber() {
		return number;
	}
	/**
	 * @param number the number to set
	 */
	public void setNumber(String number) {
		this.number = number;
	}
	/**
	 * @return the institutionId
	 */
	public Integer getInstitutionId() {
		return institutionId;
	}
	/**
	 * @param institutionId the institutionId to set
	 */
	public void setInstitutionId(Integer institutionId) {
		this.institutionId = institutionId;
	}
	/**
	 * @return the classId
	 */
	public Integer getClassId() {
		return classId;
	}
	/**
	 * @param classId the classId to set
	 */
	public void setClassId(Integer classId) {
		this.classId = classId;
	}
	/**
	 * @return the role
	 */
	public Integer getRole() {
		return role;
	}
	/**
	 * @param role the role to set
	 */
	public void setRole(Integer role) {
		this.role = role;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the deviceModifyDate
	 */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	public Date getDeviceModifyDate() {
		return deviceModifyDate;
	}
	/**
	 * @param deviceModifyDate the deviceModifyDate to set
	 */
	public void setDeviceModifyDate(Date deviceModifyDate) {
		this.deviceModifyDate = deviceModifyDate;
	}
	/**
	 * @return the sex
	 */
	public Integer getSex() {
		return sex;
	}
	/**
	 * @param sex the sex to set
	 */
	public void setSex(Integer sex) {
		this.sex = sex;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the deviceNumber
	 */
	public String getDeviceNumber() {
		return deviceNumber;
	}
	/**
	 * @param deviceNumber the deviceNumber to set
	 */
	public void setDeviceNumber(String deviceNumber) {
		this.deviceNumber = deviceNumber;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "User [id=" + id + ", number=" + number + ", institutionId=" + institutionId + ", classId=" + classId
				+ ", role=" + role + ", name=" + name + ", password=" + password + ", deviceModifyDate="
				+ deviceModifyDate + ", sex=" + sex + ", email=" + email + ", deviceNumber=" + deviceNumber + "]";
	}
    
}
