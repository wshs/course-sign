package signHelper.modle;

import java.util.Date;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;

import com.fasterxml.jackson.annotation.JsonFormat;

public class Temp {
	
	@Range(min=1,max=10,message="{test.id.error}")
	private Integer id;  
	@NotBlank(message = "{test.name.empty}")  
	@Size(min=5, max=20, message="{test.name.length.error}")  
	@Pattern(regexp = "^[a-zA-Z_]\\w{4,19}$", message = "{test.name.pattern.error}")
    private String name;
	private String password;
    private Date date;
    
    
	/**
	 * @return the date
	 */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	public Date getDate() {
		return date;
	}
	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", password=" + password + ", createDate=" + date + "]";
	}

}
