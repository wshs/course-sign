package signHelper.modle;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class CourseRecord {
	
	private Integer id;  
	private Integer courseId;
    private Date startTime;
    private Date endTime;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getCourseId() {
		return courseId;
	}
	public void setCourseId(Integer courseId) {
		this.courseId = courseId;
	}
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	@Override
	public String toString() {
		return "CourseRecord [id=" + id + ", courseId=" + courseId + ", startTime=" + startTime + ", endTime=" + endTime
				+ "]";
	}
	
}
