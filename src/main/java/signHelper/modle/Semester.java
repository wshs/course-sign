package signHelper.modle;

import java.time.LocalDate;

public class Semester {
	
	private Integer id;  
	private Integer schoolId;  
	private String name;
	private LocalDate startDate;
	private LocalDate endDate;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(Integer schoolId) {
		this.schoolId = schoolId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public LocalDate getStartDate() {
		return startDate;
	}
	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}
	public LocalDate getEndDate() {
		return endDate;
	}
	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}
	@Override
	public String toString() {
		return "Semester [id=" + id + ", schoolId=" + schoolId + ", name=" + name + ", startDate=" + startDate
				+ ", endDate=" + endDate + "]";
	}
	
    
}
