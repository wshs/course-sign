package signHelper.service.validation;

import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import signHelper.modle.User;
import signHelper.mvc.ApiException;
import signHelper.service.UserService;

/**
 * 
 * @author <a href="mailto:1360527082@qq.com">王焕</a>
 * @since 0.1.0
 */

@Component
public class UserValidator extends BaseValidator {
	protected final Logger log = LogManager.getLogger(getClass());
	private @Autowired UserService service;

	public void validateAdd(User obj) {
		validateObjectNull(obj,"user");
		validateObjectNull(obj.getName(),"user.name");
		validateObjectNull(obj.getNumber(),"user.number");
		validateObjectNull(obj.getRole(),"user.role");
		if (obj.getRole().intValue() == 1)
			validateObjectNull(obj.getInstitutionId(),"user.institutionId");
		if (obj.getRole().intValue() == 2)
			validateObjectNull(obj.getClassId(),"user.classId");
		validateObjectNull(obj.getPassword(),"user.password");
		validateObjectNull(obj.getDeviceNumber(),"user.deviceNumber");
		obj.setDeviceModifyDate(new Date());
		
		User param=new User();
		param.setNumber(obj.getNumber());
		if(!service.query(param, 0, 1).isEmpty())
			throw new ApiException("number.exist", "学号/工号已存在");
		param.setNumber(null);
		param.setDeviceNumber(obj.getDeviceNumber());
		if(!service.query(param, 0, 1).isEmpty())
			throw new ApiException("deviceNumber.exist", "用户设备已绑定");
		
	}

	public void validateUpdate(User obj) {

		validateObjectNull(obj,"user");
		validateObjectNull(obj.getId(),"user.id");

		User src = service.getById(obj.getId());
		obj.setRole(obj.getRole() == null ? src.getRole() : obj.getRole());
		if (obj.getRole().intValue() == 1) { // tech
			obj.setClassId(null);
			obj.setInstitutionId(obj.getInstitutionId() == null ? src.getInstitutionId() : obj.getInstitutionId());
			validateObjectNull(obj.getInstitutionId(),"user.institutionId");
		}
		if (obj.getRole().intValue() == 2) {
			obj.setInstitutionId(null);
			obj.setClassId(obj.getClassId() == null ? src.getClassId() : obj.getClassId());
			validateObjectNull(obj.getClassId(),"user.classId");
		}
		if(obj.getDeviceNumber()!=null&&!obj.getDeviceNumber().equals(src.getDeviceNumber()))
			obj.setDeviceModifyDate(new Date());
		
		User param=new User();
		if(!src.getNumber().equals(obj.getNumber())){
			param.setNumber(obj.getNumber());
			if(!service.query(param, 0, 1).isEmpty())
				throw new ApiException("number.exist", "学号/工号已存在");
		}
		
		if(obj.getDeviceNumber()!=null&&!obj.getDeviceNumber().equals(src.getDeviceNumber())){
			if(DateUtils.addDays(src.getDeviceModifyDate(), 7).after(new Date()))
				throw new ApiException("deviceNumber.change.error", "7天内只允许修改一次用户设备");
			param.setNumber(null);
			param.setDeviceNumber(obj.getDeviceNumber());
			if(!service.query(param, 0, 1).isEmpty())
				throw new ApiException("deviceNumber.exist", "用户设备已绑定");
			obj.setDeviceModifyDate(new Date());
		}
		
		
	}
}
