package signHelper.service.validation;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import signHelper.mvc.ApiException;
import signHelper.util.UtilJson;

/**
 * 
 * @author <a href="mailto:1360527082@qq.com">王焕</a>
 * @since 0.1.0
 */


public abstract class BaseValidator {
	protected final Logger log = LogManager.getLogger(getClass());
	public void validateObjectNull(Object obj,String otherMsg) {
		if (obj == null) {
            log.error("对象不能为空,otherMsg={}", otherMsg);
            throw new ApiException("object."+otherMsg+".null",otherMsg+" cant'be null");
        }
	}
	
	public void throwUniqeExeception(Object obj){
		throw new ApiException("uniqe",UtilJson.writeValueAsString(obj));
	}
}
