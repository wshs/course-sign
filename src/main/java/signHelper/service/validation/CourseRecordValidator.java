package signHelper.service.validation;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import signHelper.modle.CourseRecord;

/**
 * 
 * @author <a href="mailto:1360527082@qq.com">王焕</a>
 * @since 0.1.0
 */

@Component
public class CourseRecordValidator extends BaseValidator {
	protected final Logger log = LogManager.getLogger(getClass());

	public void validateAdd(CourseRecord obj) {
		validateObjectNull(obj,"courseRecord");
		validateObjectNull(obj.getCourseId(),"courseRecord.courseId");
		validateObjectNull(obj.getEndTime(),"courseRecord.endtime");
		validateObjectNull(obj.getStartTime(),"courseRecord.starttime");
	}

	public void validateUpdate(CourseRecord obj) {
		validateObjectNull(obj,"courseRecord");
		validateObjectNull(obj.getId(),"courseRecord.id");
	}
	
}
