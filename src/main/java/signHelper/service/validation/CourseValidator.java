package signHelper.service.validation;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import signHelper.modle.Course;

/**
 * 
 * @author <a href="mailto:1360527082@qq.com">王焕</a>
 * @since 0.1.0
 */

@Component
public class CourseValidator extends BaseValidator {
	protected final Logger log = LogManager.getLogger(getClass());

	public void validateAdd(Course obj) {
		validateObjectNull(obj,"course");
		validateObjectNull(obj.getName(),"course.name");
		validateObjectNull(obj.getCoordinate(),"course.coordinate");
		validateObjectNull(obj.getDescription(),"course.desc");
		validateObjectNull(obj.getEndWeek(),"course.endweek");
		validateObjectNull(obj.getInstitutionId(),"course.institutionId");
		validateObjectNull(obj.getLocation(),"course.location");
		validateObjectNull(obj.getSemesterId(),"course.semesterId");
		validateObjectNull(obj.getStartWeek(),"course.startWeek");
		validateObjectNull(obj.getTeacherId(),"course.teacherId");
		
	}

	public void validateUpdate(Course obj) {

		validateObjectNull(obj,"course");
		validateObjectNull(obj.getId(),"course.id");
	}
	
}
