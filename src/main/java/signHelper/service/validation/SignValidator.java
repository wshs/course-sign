package signHelper.service.validation;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import signHelper.modle.Sign;

/**
 * 
 * @author <a href="mailto:1360527082@qq.com">王焕</a>
 * @since 0.1.0
 */

@Component
public class SignValidator extends BaseValidator {
	protected final Logger log = LogManager.getLogger(getClass());

	public void validateAdd(Sign obj) {
		validateObjectNull(obj,"sign");
		validateObjectNull(obj.getCourseRecordId(),"sign.courseRecordId");
		validateObjectNull(obj.getStuId(),"sign.stuId");
		validateObjectNull(obj.getType(),"sign.type");
	}

	public void validateUpdate(Sign obj) {
		validateObjectNull(obj,"sign");
		validateObjectNull(obj.getId(),"sign.id");
	}
	
}
