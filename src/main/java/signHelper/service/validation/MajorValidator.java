package signHelper.service.validation;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import signHelper.modle.Major;

/**
 * 
 * @author <a href="mailto:1360527082@qq.com">王焕</a>
 * @since 0.1.0
 */

@Component
public class MajorValidator extends BaseValidator{
	protected final Logger log = LogManager.getLogger(getClass());

	public void validateAdd(Major obj) {
		validateObjectNull(obj,"major");
		validateObjectNull(obj.getName(),"major.name");
		validateObjectNull(obj.getInstitutionId(),"major.institutionId");
	}

	public void validateUpdate(Major bean) {
		validateObjectNull(bean,"major");
		validateObjectNull(bean.getId(),"major.id");
	}
}
