package signHelper.service.validation;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import signHelper.modle.School;

/**
 * 
 * @author <a href="mailto:1360527082@qq.com">王焕</a>
 * @since 0.1.0
 */

@Component
public class SchoolValidator extends BaseValidator{
	protected final Logger log = LogManager.getLogger(getClass());

	public void validateAdd(School obj) {
		validateObjectNull(obj,"school");
		validateObjectNull(obj.getName(),"school.name");
	}

	public void validateUpdate(School bean) {
		validateObjectNull(bean,"school");
		validateObjectNull(bean.getId(),"school.id");
	}
}
