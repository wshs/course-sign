package signHelper.service.validation;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import signHelper.modle.Semester;

/**
 * 
 * @author <a href="mailto:1360527082@qq.com">王焕</a>
 * @since 0.1.0
 */

@Component
public class SemesterValidator extends BaseValidator{
	protected final Logger log = LogManager.getLogger(getClass());

	public void validateAdd(Semester obj) {
		validateObjectNull(obj,"semester");
		validateObjectNull(obj.getName(),"semester.name");
		validateObjectNull(obj.getSchoolId(),"semester.schoolId");
		validateObjectNull(obj.getStartDate(),"semester.startdate");
		validateObjectNull(obj.getEndDate(),"semester.enddate");
	}

	public void validateUpdate(Semester bean) {
		validateObjectNull(bean,"semester");
		validateObjectNull(bean.getId(),"semester.id");
	}
}
