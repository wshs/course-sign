package signHelper.service.validation;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import signHelper.modle.Class;

/**
 * 
 * @author <a href="mailto:1360527082@qq.com">王焕</a>
 * @since 0.1.0
 */

@Component
public class ClassValidator extends BaseValidator{
	protected final Logger log = LogManager.getLogger(getClass());

	public void validateAdd(Class obj) {
		validateObjectNull(obj,"class");
		validateObjectNull(obj.getName(),"class.name");
		validateObjectNull(obj.getMajorId(),"class.majorId");
	}

	public void validateUpdate(Class bean) {
		validateObjectNull(bean,"class");
		validateObjectNull(bean.getId(),"class.id");
		
	}
}
