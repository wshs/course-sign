package signHelper.service.validation;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import signHelper.modle.Institution;

/**
 * 
 * @author <a href="mailto:1360527082@qq.com">王焕</a>
 * @since 0.1.0
 */

@Component
public class InstitutionValidator extends BaseValidator{
	protected final Logger log = LogManager.getLogger(getClass());

	public void validateAdd(Institution obj) {
		validateObjectNull(obj,"institution");
		validateObjectNull(obj.getName(),"institution.name");
		validateObjectNull(obj.getSchoolId(),"institution.schoolId");
	}

	public void validateUpdate(Institution bean) {
		validateObjectNull(bean,"institution");
		validateObjectNull(bean.getId(),"institution.id");
	}
}
