package signHelper.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import signHelper.dao.MajorDao;
import signHelper.modle.Major;
import signHelper.service.validation.MajorValidator;

@Service
public class MajorService {
	private @Autowired MajorDao majorDao;
	private @Autowired MajorValidator validator;
	
	public Major add(Major bean) {
		validator.validateAdd(bean);
		return majorDao.add(bean);
	}
	
	public Major getById(Integer id){
		validator.validateObjectNull(id,"major.id");
		return majorDao.getById(id);
	}
	
	public List<Major> query(Major bean,Integer has,Integer nextSize) {
		validator.validateObjectNull(bean,"major");
		return majorDao.query(bean, has, nextSize);
	}
	
	public void update(Major bean) {
		validator.validateUpdate(bean);
		majorDao.update(bean);
	}
	
	public void delete(Integer id) {
		validator.validateObjectNull(id,"major.id");
		majorDao.delete(id);
	}
	
}
