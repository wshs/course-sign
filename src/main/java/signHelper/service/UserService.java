package signHelper.service;

import java.util.List;

import org.apache.shiro.crypto.hash.Md5Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import signHelper.dao.UserDao;
import signHelper.modle.User;
import signHelper.service.validation.UserValidator;

@Service
public class UserService {
	private @Autowired UserDao userDao;
	private @Autowired UserValidator validator;

	public User add(User bean) {
		validator.validateAdd(bean);
		if (bean.getPassword() != null)
			bean.setPassword((new Md5Hash(bean.getPassword())).toString());
		return userDao.add(bean);
	}

	public User getById(Integer id) {
		validator.validateObjectNull(id,"user.id");
		return userDao.getById(id);
	}

	public List<User> query(User bean, Integer has, Integer nextSize) {
		validator.validateObjectNull(bean,"user");
		return userDao.query(bean, has, nextSize);
	}

	public void update(User bean) {
		validator.validateUpdate(bean);
		if (bean.getPassword() != null)
			bean.setPassword((new Md5Hash(bean.getPassword())).toString());
		userDao.update(bean);
	}

	public void delete(Integer id) {
		validator.validateObjectNull(id,"user.id");
		userDao.delete(id);
	}

}
