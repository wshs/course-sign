package signHelper.service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import signHelper.dao.CourseDao;
import signHelper.modle.Course;
import signHelper.modle.CourseRecord;
import signHelper.modle.Semester;
import signHelper.mvc.modle.AppCourse;
import signHelper.service.validation.CourseValidator;
import signHelper.util.UtilDate;

@Service
public class CourseService {
	private @Autowired CourseDao courseDao;
	private @Autowired CourseRecordService courseRecordService;
	private @Autowired CourseValidator validator;
	private @Autowired SemesterService semesterService;
	
	@Transactional
	public Course add(Course bean) {
		validator.validateAdd(bean);
		/**生成课程记录*/
		AppCourse appCourse=new AppCourse();
		courseDao.add(bean);
		appCourse.fromCourse(bean);
		Semester semester=semesterService.getById(bean.getSemesterId());
		LocalDate semesterStartTime=semester.getStartDate();
		LocalDate semesterEndTime=semester.getEndDate();
		List<AppCourse.CourseTime> oddTimes=appCourse.getOddTimes();
		List<AppCourse.CourseTime> evenTimes=appCourse.getEvenTimes();
		LocalDate startDay=semesterStartTime.plusWeeks(appCourse.getStartWeek()-1);
		LocalDate endDay=semesterStartTime.plusWeeks(appCourse.getEndWeek()-1);
		LocalDate startOddDay=startDay, startEvenDay=startDay;
		if(appCourse.getStartWeek()%2==0){
			startOddDay=startDay.plusWeeks(1);
		}else{
			startEvenDay=startDay.plusWeeks(1);
		}
		/**生成奇数周课程记录*/
		for(AppCourse.CourseTime time:oddTimes){
			LocalDate temp=startOddDay.minusDays(1);
			temp=UtilDate.nextWhichDay(temp, DayOfWeek.of(time.getDay()));
			while (temp.isBefore(endDay)) {
				CourseRecord record=new CourseRecord();
				record.setCourseId(appCourse.getId());
				record.setStartTime(UtilDate.LocalDateTimeToUdate(temp,time.getStart()));
				record.setEndTime(UtilDate.LocalDateTimeToUdate(temp,time.getEnd()));
				courseRecordService.add(record);
				temp=temp.plusWeeks(2);
			}
		};
		/**生成偶数周课程记录*/
		for(AppCourse.CourseTime time:evenTimes){
			LocalDate temp=startEvenDay.minusDays(1);
			temp=UtilDate.nextWhichDay(temp, DayOfWeek.of(time.getDay()));
			while (temp.isBefore(endDay)) {
				CourseRecord record=new CourseRecord();
				record.setCourseId(appCourse.getId());
				record.setStartTime(UtilDate.LocalDateTimeToUdate(temp,time.getStart()));
				record.setEndTime(UtilDate.LocalDateTimeToUdate(temp,time.getEnd()));
				courseRecordService.add(record);
				temp=temp.plusWeeks(2);
			}
		};
		return bean;
	}
	
	public Course getById(Integer id){
		validator.validateObjectNull(id,"course");
		return courseDao.getById(id);
	}
	
	public List<Course> query(Course bean,Integer has,Integer nextSize) {
		validator.validateObjectNull(bean,"course");
		return courseDao.query(bean, has, nextSize);
	}
	
	public void update(Course bean) {
		validator.validateUpdate(bean);
		courseDao.update(bean);
	}
	
	public void delete(Integer id) {
		validator.validateObjectNull(id,"course.id");
		courseDao.delete(id);
	}
	
	public List<Integer> addStuCourseRel(List<Integer> ids,Integer courseId) {
		validator.validateObjectNull(ids,"course.ids");
		validator.validateObjectNull(courseId,"course.courseId");
		List<Integer> errors=new ArrayList<>();
		for (Integer stuId : ids) {
			try {
				courseDao.addStuCourseRel(stuId, courseId);
			} catch (Exception e) {
				errors.add(stuId);
			}
		}
		return errors;
	}
	public void delStuCourseRel(Integer stuId,Integer courseId) {
		validator.validateObjectNull(stuId,"stuId");
		validator.validateObjectNull(courseId,"courseId");
		courseDao.delStuCourseRel(stuId, courseId);
	}

	public List<Course> findCurrentDayCourse(Integer userId, boolean isStu) {
		// TODO Auto-generated method stub
		return null;
	}

	public Course findCurrentCourse(Integer userId, boolean isStu) {
		// TODO Auto-generated method stub
		return null;
	}
}
