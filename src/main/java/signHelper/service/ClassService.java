package signHelper.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import signHelper.dao.ClassDao;
import signHelper.modle.Class;
import signHelper.service.validation.ClassValidator;

@Service
public class ClassService {
	private @Autowired ClassDao classDao;
	private @Autowired ClassValidator validator;
	
	public Class add(Class bean) {
		validator.validateAdd(bean);
		return classDao.add(bean);
	}
	
	public Class getById(Integer id){
		validator.validateObjectNull(id,"class");
		return classDao.getById(id);
	}
	
	public List<Class> query(Class bean,Integer has,Integer nextSize) {
		validator.validateObjectNull(bean,"class");
		return classDao.query(bean, has, nextSize);
	}
	
	public void update(Class bean) {
		validator.validateUpdate(bean);
		classDao.update(bean);
	}
	
	public void delete(Integer id) {
		validator.validateObjectNull(id,"class.id");
		classDao.delete(id);
	}
	
}
