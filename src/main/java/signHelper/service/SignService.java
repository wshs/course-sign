package signHelper.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import signHelper.dao.SignDao;
import signHelper.modle.Sign;
import signHelper.service.validation.SignValidator;

@Service
public class SignService {
	private @Autowired SignDao signDao;
	private @Autowired SignValidator validator;
	
	public Sign add(Sign bean) {
		validator.validateAdd(bean);
		return signDao.add(bean);
	}
	
	public Sign getById(Integer id){
		validator.validateObjectNull(id,"sign.id");
		return signDao.getById(id);
	}
	
	public List<Sign> query(Sign bean,Integer has,Integer nextSize) {
		validator.validateObjectNull(bean,"sign");
		return signDao.query(bean, has, nextSize);
	}
	
	public void update(Sign bean) {
		validator.validateUpdate(bean);
		signDao.update(bean);
	}
	
	public void delete(Integer id) {
		validator.validateObjectNull(id,"sign.id");
		signDao.delete(id);
	}
	
}
