package signHelper.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import signHelper.dao.SemesterDao;
import signHelper.modle.Semester;
import signHelper.service.validation.SemesterValidator;

@Service
public class SemesterService {
	private @Autowired SemesterDao semesterDao;
	private @Autowired SemesterValidator validator;
	
	public Semester add(Semester bean) {
		validator.validateAdd(bean);
		return semesterDao.add(bean);
	}
	
	public Semester getById(Integer id){
		validator.validateObjectNull(id,"semester.id");
		return semesterDao.getById(id);
	}
	
	public List<Semester> query(Semester bean,Integer has,Integer nextSize) {
		validator.validateObjectNull(bean,"semester");
		return semesterDao.query(bean, has, nextSize);
	}
	
	public void update(Semester bean) {
		validator.validateUpdate(bean);
		semesterDao.update(bean);
	}
	
	public void delete(Integer id) {
		validator.validateObjectNull(id,"semester.id");
		semesterDao.delete(id);
	}
	
}
