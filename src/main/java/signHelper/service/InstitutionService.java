package signHelper.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import signHelper.dao.InstitutionDao;
import signHelper.modle.Institution;
import signHelper.service.validation.InstitutionValidator;

@Service
public class InstitutionService {
	private @Autowired InstitutionDao institutionDao;
	private @Autowired InstitutionValidator validator;
	
	public Institution add(Institution bean) {
		validator.validateAdd(bean);
		return institutionDao.add(bean);
	}
	
	public Institution getById(Integer id){
		validator.validateObjectNull(id,"institution.id");
		return institutionDao.getById(id);
	}
	
	public List<Institution> query(Institution bean,Integer has,Integer nextSize) {
		validator.validateObjectNull(bean,"institution");
		return institutionDao.query(bean, has, nextSize);
	}
	
	public void update(Institution bean) {
		validator.validateUpdate(bean);
		institutionDao.update(bean);
	}
	
	public void delete(Integer id) {
		validator.validateObjectNull(id,"institution.id");
		institutionDao.delete(id);
	}
	
}
