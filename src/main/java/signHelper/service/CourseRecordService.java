package signHelper.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import signHelper.dao.CourseRecordDao;
import signHelper.modle.CourseRecord;
import signHelper.service.validation.CourseRecordValidator;

@Service
public class CourseRecordService {
	private @Autowired CourseRecordDao courseRecordDao;
	private @Autowired CourseRecordValidator validator;
	
	public CourseRecord add(CourseRecord bean) {
		validator.validateAdd(bean);
		return courseRecordDao.add(bean);
	}
	
	public CourseRecord getById(Integer id){
		validator.validateObjectNull(id,"courseRecord.id");
		return courseRecordDao.getById(id);
	}
	
	public List<CourseRecord> query(CourseRecord bean,Integer has,Integer nextSize) {
		validator.validateObjectNull(bean,"courseRecord");
		return courseRecordDao.query(bean, has, nextSize);
	}
	
	public void update(CourseRecord bean) {
		validator.validateUpdate(bean);
		courseRecordDao.update(bean);
	}
	
	public void delete(Integer id) {
		validator.validateObjectNull(id,"courseRecord.id");
		courseRecordDao.delete(id);
	}
	
}
