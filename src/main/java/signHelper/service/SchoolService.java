package signHelper.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import signHelper.dao.SchoolDao;
import signHelper.modle.School;
import signHelper.service.validation.SchoolValidator;

@Service
public class SchoolService {
	private @Autowired SchoolDao schoolDao;
	private @Autowired SchoolValidator validator;
	
	public School add(School bean) {
		validator.validateAdd(bean);
		return schoolDao.add(bean);
	}
	
	public School getById(Integer id){
		validator.validateObjectNull(id,"school.id");
		return schoolDao.getById(id);
	}
	
	public List<School> query(School bean,Integer has,Integer nextSize) {
		validator.validateObjectNull(bean,"school");
		return schoolDao.query(bean, has, nextSize);
	}
	
	public void update(School bean) {
		validator.validateUpdate(bean);
		schoolDao.update(bean);
	}
	
	public void delete(Integer id) {
		validator.validateObjectNull(id,"school.id");
		schoolDao.delete(id);
	}
	
}
