package signHelper.mvc.modle;

import java.time.LocalTime;
import java.util.List;

import org.springframework.beans.BeanUtils;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import signHelper.modle.Course;
import signHelper.util.UtilJson;


public class AppCourse{
	
	
	private Integer id;  
	private Integer institutionId;
	private Integer teacherId;
    private String name;
    private String location;
    private String coordinate;
	private String description;
    private Integer semesterId;
    private Integer startWeek;
    private Integer endWeek;
    
    private List<CourseTime> oddTimes;
    private List<CourseTime> evenTimes;
    
    public Course toCourse(){
    	Course course=new Course();
    	BeanUtils.copyProperties(this, course);
    	course.setOddTime(UtilJson.writeValueAsString(oddTimes));
    	course.setEvenTime(UtilJson.writeValueAsString(evenTimes));
    	return course;
    }
    
    public void fromCourse(Course course){
    	BeanUtils.copyProperties(course, this);
    	oddTimes=UtilJson.readList(course.getOddTime(), CourseTime.class);
    	evenTimes=UtilJson.readList(course.getEvenTime(), CourseTime.class);
    }
    
    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getInstitutionId() {
		return institutionId;
	}

	public void setInstitutionId(Integer institutionId) {
		this.institutionId = institutionId;
	}

	public Integer getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Integer teacherId) {
		this.teacherId = teacherId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getCoordinate() {
		return coordinate;
	}

	public void setCoordinate(String coordinate) {
		this.coordinate = coordinate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getSemesterId() {
		return semesterId;
	}

	public void setSemesterId(Integer semesterId) {
		this.semesterId = semesterId;
	}

	public Integer getStartWeek() {
		return startWeek;
	}

	public void setStartWeek(Integer startWeek) {
		this.startWeek = startWeek;
	}

	public Integer getEndWeek() {
		return endWeek;
	}

	public void setEndWeek(Integer endWeek) {
		this.endWeek = endWeek;
	}

	public List<CourseTime> getOddTimes() {
		return oddTimes;
	}

	public void setOddTimes(List<CourseTime> oddTimes) {
		this.oddTimes = oddTimes;
	}

	public List<CourseTime> getEvenTimes() {
		return evenTimes;
	}

	public void setEvenTimes(List<CourseTime> evenTimes) {
		this.evenTimes = evenTimes;
	}
	@JsonDeserialize(using=UtilJson.AppCourseTimeDeserializer.class)
	public static class CourseTime{
    	/** 星期 1-7*/
    	Integer day;
    	LocalTime start;
    	LocalTime end;
		public Integer getDay() {
			return day;
		}
		public void setDay(Integer day) {
			this.day = day;
		}
		public LocalTime getStart() {
			return start;
		}
		public void setStart(LocalTime start) {
			this.start = start;
		}
		public LocalTime getEnd() {
			return end;
		}
		public void setEnd(LocalTime end) {
			this.end = end;
		}
		@Override
		public String toString() {
			return "CourseTime [day=" + day + ", start=" + start + ", end=" + end + "]";
		}
    	
    }

	@Override
	public String toString() {
		return "AppCourse [id=" + id + ", institutionId=" + institutionId + ", teacherId=" + teacherId + ", name="
				+ name + ", location=" + location + ", coordinate=" + coordinate + ", description=" + description
				+ ", semesterId=" + semesterId + ", startWeek=" + startWeek + ", endWeek=" + endWeek + ", oddTimes="
				+ oddTimes + ", evenTimes=" + evenTimes + "]";
	}

}
