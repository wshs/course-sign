package signHelper.mvc.controller;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import signHelper.modle.Semester;
import signHelper.service.SemesterService;

@Controller
@RequestMapping("/semester")
public class SemesterController {

	private final Logger logger = LogManager.getLogger(getClass());
	@Autowired
	private SemesterService service;

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@ApiOperation(value = "新增学期")
	@ResponseBody
	public String add(@ApiParam(required = true, value = "学期信息") @RequestBody Semester semester) throws Exception {
		service.add(semester);
		return "success";
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ApiOperation(value = "修改学期")
	@ResponseBody
	public String update(@ApiParam(required = true, value = "学期信息") @RequestBody Semester semester) throws Exception {
		service.update(semester);
		return "success";
	}

	@ApiOperation(value = "删除学期")
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public String del(@ApiParam(required = true, value = "id") @PathVariable Integer id) {
		service.delete(id);
		return "success";
	}
	
	@ApiOperation(value = "根据id获取学期")
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Semester getById(@ApiParam(required = true, value = "id") @PathVariable Integer id) {
		return service.getById(id);
	}

	@ApiOperation(value = "查找学期")
	@RequestMapping(value = "/find", method = RequestMethod.GET)
	@ResponseBody
	public List<Semester> find(@ApiParam(required = true, value = "学期信息") @RequestBody Semester semester,
			@ApiParam(required = true, value = "已有") @RequestParam("has") Integer has,
			@ApiParam(required = true, value = "加载数量") @RequestParam("nextSize") Integer nextSize) {
		return service.query(semester, has, nextSize);
	}

}