package signHelper.mvc.controller;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import signHelper.modle.CourseRecord;
import signHelper.service.CourseRecordService;

@Controller
@RequestMapping("/courseRecord")
public class CourseRecordController {

	private final Logger logger = LogManager.getLogger(getClass());
	@Autowired
	private CourseRecordService service;

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@ApiOperation(value = "新增上课记录")
	@ResponseBody
	public String add(@ApiParam(required = true, value = "上课记录信息") @RequestBody CourseRecord courseRecord) throws Exception {
		service.add(courseRecord);
		return "success";
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ApiOperation(value = "修改上课记录")
	@ResponseBody
	public String update(@ApiParam(required = true, value = "上课记录信息") @RequestBody CourseRecord courseRecord) throws Exception {
		service.update(courseRecord);
		return "success";
	}

	@ApiOperation(value = "删除上课记录")
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public String del(@ApiParam(required = true, value = "id") @PathVariable Integer id) {
		service.delete(id);
		return "success";
	}
	
	@ApiOperation(value = "根据id获取上课记录")
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public CourseRecord getById(@ApiParam(required = true, value = "id") @PathVariable Integer id) {
		return service.getById(id);
	}

	@ApiOperation(value = "查找上课记录")
	@RequestMapping(value = "/find", method = RequestMethod.GET)
	@ResponseBody
	public List<CourseRecord> find(@ApiParam(required = true, value = "上课记录信息") @RequestBody CourseRecord courseRecord,
			@ApiParam(required = true, value = "已有") @RequestParam("has") Integer has,
			@ApiParam(required = true, value = "加载数量") @RequestParam("nextSize") Integer nextSize) {
		return service.query(courseRecord, has, nextSize);
	}

}