package signHelper.mvc.controller;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import signHelper.modle.Class;
import signHelper.modle.Institution;
import signHelper.service.ClassService;

@Controller
@RequestMapping("/class")
public class ClassController {

	private final Logger logger = LogManager.getLogger(getClass());
	@Autowired
	private ClassService service;

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@ApiOperation(value = "新增班级")
	@ResponseBody
	public String add(@ApiParam(required = true, value = "班级信息") @RequestBody Class clazz) throws Exception {
		service.add(clazz);
		return "success";
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ApiOperation(value = "修改班级")
	@ResponseBody
	public String update(@ApiParam(required = true, value = "班级信息") @RequestBody Class clazz) throws Exception {
		service.update(clazz);
		return "success";
	}

	@ApiOperation(value = "删除班级")
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public String del(@ApiParam(required = true, value = "id") @PathVariable Integer id) {
		service.delete(id);
		return "success";
	}
	
	@ApiOperation(value = "根据id获取班级")
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Class getById(@ApiParam(required = true, value = "id") @PathVariable Integer id) {
		return service.getById(id);
	}

	@ApiOperation(value = "查找班级")
	@RequestMapping(value = "/find", method = RequestMethod.GET)
	@ResponseBody
	public List<Class> find(@ApiParam(required = true, value = "班级信息") @RequestBody Class clazz,
			@ApiParam(required = true, value = "已有") @RequestParam("has") Integer has,
			@ApiParam(required = true, value = "加载数量") @RequestParam("nextSize") Integer nextSize) {
		return service.query(clazz, has, nextSize);
	}
	
	@ApiOperation(value = "根据专业id查找班级")
	@RequestMapping(value = "/findByMajorId", method = RequestMethod.GET)
	@ResponseBody
	public List<Class> findByMajorId(@ApiParam(required = true, value = "专业id") @RequestParam Integer majorId,
			@ApiParam(required = true, value = "已有") @RequestParam("has") Integer has,
			@ApiParam(required = true, value = "加载数量") @RequestParam("nextSize") Integer nextSize) {
		Class clazz=new Class();
		clazz.setMajorId(majorId);;
		return service.query(clazz, has, nextSize);
	}
	

}