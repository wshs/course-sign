package signHelper.mvc.controller;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import signHelper.modle.Class;
import signHelper.modle.Major;
import signHelper.service.MajorService;

@Controller
@RequestMapping("/major")
public class MajorController {

	private final Logger logger = LogManager.getLogger(getClass());
	@Autowired
	private MajorService service;

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@ApiOperation(value = "新增专业")
	@ResponseBody
	public String add(@ApiParam(required = true, value = "专业信息") @RequestBody Major major) throws Exception {
		service.add(major);
		return "success";
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ApiOperation(value = "修改专业")
	@ResponseBody
	public String update(@ApiParam(required = true, value = "专业信息") @RequestBody Major major) throws Exception {
		service.update(major);
		return "success";
	}

	@ApiOperation(value = "删除专业")
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public String del(@ApiParam(required = true, value = "id") @PathVariable Integer id) {
		service.delete(id);
		return "success";
	}
	
	@ApiOperation(value = "根据id获取专业")
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Major getById(@ApiParam(required = true, value = "id") @PathVariable Integer id) {
		return service.getById(id);
	}

	@ApiOperation(value = "查找专业")
	@RequestMapping(value = "/find", method = RequestMethod.GET)
	@ResponseBody
	public List<Major> find(@ApiParam(value = "专业信息") @RequestBody Major major,
			@ApiParam(required = true, value = "已有") @RequestParam("has") Integer has,
			@ApiParam(required = true, value = "加载数量") @RequestParam("nextSize") Integer nextSize) {
		return service.query(major, has, nextSize);
	}
	
	@ApiOperation(value = "根据学院id查找专业")
	@RequestMapping(value = "/findByInstitutionId", method = RequestMethod.GET)
	@ResponseBody
	public List<Major> findByInstitutionId(@ApiParam(required = true, value = "学院id") @RequestParam Integer institutionId,
			@ApiParam(required = true, value = "已有") @RequestParam("has") Integer has,
			@ApiParam(required = true, value = "加载数量") @RequestParam("nextSize") Integer nextSize) {
		Major clazz=new Major();
		clazz.setInstitutionId(institutionId);;
		return service.query(clazz, has, nextSize);
	}

}