package signHelper.mvc.controller;

import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import signHelper.mvc.modle.AppCourse;
import signHelper.modle.Course;
import signHelper.mvc.ApiException;
import signHelper.service.CourseService;

@Controller
@RequestMapping("/course")
public class CourseController {

	private final Logger logger = LogManager.getLogger(getClass());
	@Autowired
	private CourseService service;

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@ApiOperation(value = "新增课程")
	@ResponseBody
	public String add(@ApiParam(required = true, value = "课程信息") @RequestBody AppCourse course) throws Exception {
		service.add(course.toCourse());
		return "success";
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ApiOperation(value = "修改课程")
	@ResponseBody
	public String update(@ApiParam(required = true, value = "课程信息") @RequestBody AppCourse course) throws Exception {
		service.update(course.toCourse());
		return "success";
	}

	@ApiOperation(value = "删除课程")
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public String del(@ApiParam(required = true, value = "id") @PathVariable Integer id) {
		service.delete(id);
		return "success";
	}

	@ApiOperation(value = "根据id获取课程")
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public AppCourse getById(@ApiParam(required = true, value = "id") @PathVariable Integer id) {
		AppCourse ac = new AppCourse();
		ac.fromCourse(service.getById(id));
		return ac;
	}

	@ApiOperation(value = "查找课程")
	@RequestMapping(value = "/find", method = RequestMethod.GET)
	@ResponseBody
	public List<AppCourse> find(@ApiParam(required = true, value = "课程信息") @RequestBody AppCourse course,
			@ApiParam(required = true, value = "已有") @RequestParam("has") Integer has,
			@ApiParam(required = true, value = "加载数量") @RequestParam("nextSize") Integer nextSize) {
		List<Course> list = service.query(course.toCourse(), has, nextSize);
		List<AppCourse> ret = new ArrayList<>();
		for (Course c : list) {
			AppCourse ac = new AppCourse();
			ac.fromCourse(c);
			ret.add(ac);
		}
		return ret;
	}

	@ApiOperation(value = "新增学生课程关联,返回添加失败的学生列表")
	@RequestMapping(value = "/stuCourseRel", method = RequestMethod.PUT)
	@ResponseBody
	public List<Integer> addStuCourseRel(
			@ApiParam(required = true, value = "课程id") @RequestParam("courseId") Integer courseId,
			@ApiParam(required = true, value = "学生id列表,用英文,隔开") @RequestParam("stuIds") String stuIds) {
		ArrayList<Integer> ids = new ArrayList<>();
		try {
			for (String str : stuIds.split(",")) {
				ids.add(Integer.parseInt(str));
			}
		} catch (Exception e) {
			throw new ApiException("Error.input", "学生id列表格式有误");
		}
		return service.addStuCourseRel(ids, courseId);
	}

	@ApiOperation(value = "删除学生课程关联")
	@RequestMapping(value = "/stuCourseRel", method = RequestMethod.DELETE)
	@ResponseBody
	public String delStuCourseRel(@ApiParam(required = true, value = "课程id") @RequestParam("courseId") Integer courseId,
			@ApiParam(required = true, value = "学生id") @RequestParam("stuId") Integer stuId) {
		service.delStuCourseRel(stuId, courseId);
		return "success";
	}

	@ApiOperation(value = "查找当天课程")
	@RequestMapping(value = "/findCurrentDayCourse", method = RequestMethod.GET)
	@ResponseBody
	public List<AppCourse> findCurrentDayCourse(
			@ApiParam(required = true, value = "userId") @RequestParam(value="userId") Integer userId,
			@ApiParam(required = true, value = "isStu") @RequestParam("isStu") boolean isStu) {
		List<Course> courses=service.findCurrentDayCourse(userId,isStu);
		List<AppCourse> ret=new ArrayList<>();
		for (Course c : courses) {
			AppCourse ac=new AppCourse();
			ac.fromCourse(c);
			ret.add(ac);
		}
		return ret;
	}

	@ApiOperation(value = "查找当前课程")
	@RequestMapping(value = "/findCurrentCourse", method = RequestMethod.GET)
	@ResponseBody
	public AppCourse findCurrentCourse(
			@ApiParam(required = true, value = "userId") @RequestParam("userId") Integer userId,
			@ApiParam(required = true, value = "isStu") @RequestParam("isStu") boolean isStu) {
		AppCourse ac=new AppCourse();
		ac.fromCourse(service.findCurrentCourse(userId,isStu));
		return ac;
	}

}