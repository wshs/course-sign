/**
 * 
 */
package signHelper.mvc.controller.convert;

import java.time.LocalTime;
import java.time.format.DateTimeParseException;

import org.springframework.core.convert.converter.Converter;

import signHelper.mvc.ApiException;

/**
 * 
 * @author <a href="mailto:1360527082@qq.com">王焕</a>
 * 2017年6月10日
 */
public class LocalTimeConverter implements Converter<String, LocalTime>{

	@Override
	public LocalTime convert(String source) {
		try {
			return LocalTime.parse(source);
		} catch(DateTimeParseException e) {
			throw new ApiException(e);
		}
	}
	
}
