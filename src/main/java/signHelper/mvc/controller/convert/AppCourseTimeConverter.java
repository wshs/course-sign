/**
 * 
 */
package signHelper.mvc.controller.convert;

import java.time.format.DateTimeParseException;

import org.springframework.core.convert.converter.Converter;

import signHelper.mvc.ApiException;
import signHelper.mvc.modle.AppCourse;
import signHelper.util.UtilJson;

/**
 * 
 * @author <a href="mailto:1360527082@qq.com">王焕</a>
 * 2017年6月10日
 */
public class AppCourseTimeConverter implements Converter<String, AppCourse.CourseTime>{

	@Override
	public AppCourse.CourseTime convert(String source) {
		try {
			return UtilJson.readValue(source, AppCourse.CourseTime.class);
		} catch(DateTimeParseException e) {
			throw new ApiException(e);
		}
	}
	
}
