/**
 * 
 */
package signHelper.mvc.controller.convert;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import org.springframework.core.convert.converter.Converter;

import signHelper.mvc.ApiException;
import signHelper.util.UtilDate;

/**
 * 
 * @author <a href="mailto:1360527082@qq.com">王焕</a>
 * 2017年6月10日
 */
public class LocalDateTimeConverter implements Converter<String, LocalDateTime>{

	@Override
	public LocalDateTime convert(String source) {
		try {
			return LocalDateTime.parse(source, DateTimeFormatter.ofPattern(UtilDate.defaultPattern));
		} catch (DateTimeParseException e) {
			throw new ApiException(e);
		}
	}
	
}
