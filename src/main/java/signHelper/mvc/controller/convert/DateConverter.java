/**
 * 
 */
package signHelper.mvc.controller.convert;

import java.text.ParseException;
import java.util.Date;

import org.springframework.core.convert.converter.Converter;

import signHelper.mvc.ApiException;
import signHelper.util.UtilDate;

/**
 * 
 * @author <a href="mailto:1360527082@qq.com">王焕</a>
 * 2017年6月10日
 */
public class DateConverter implements Converter<String, Date>{

	@Override
	public Date convert(String source) {
		try {
			return UtilDate.parse(source);
		} catch (ParseException e) {
			throw new ApiException(e);
		}
	}
	
}
