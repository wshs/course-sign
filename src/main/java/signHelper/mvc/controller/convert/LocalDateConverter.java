/**
 * 
 */
package signHelper.mvc.controller.convert;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

import org.springframework.core.convert.converter.Converter;

import signHelper.mvc.ApiException;

/**
 * 
 * @author <a href="mailto:1360527082@qq.com">王焕</a>
 * 2017年6月10日
 */
public class LocalDateConverter implements Converter<String, LocalDate>{

	@Override
	public LocalDate convert(String source) {
		try {
			return LocalDate.parse(source);
		} catch (DateTimeParseException e) {
			throw new ApiException(e);
		}
	}
	
}
