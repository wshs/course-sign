package signHelper.mvc.controller;


import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import io.swagger.annotations.ApiOperation;

@Controller
public class ApiController {

	@ApiOperation(value = "重定向",hidden=true)
	@RequestMapping(value="/api",method = RequestMethod.GET)  
    public void api(HttpServletResponse response) throws IOException{  
        response.sendRedirect("/swagger/index.html");  
    }  
	
}
