package signHelper.mvc.controller;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import signHelper.modle.Sign;
import signHelper.service.SignService;

@Controller
@RequestMapping("/sign")
public class SignController {

	private final Logger logger = LogManager.getLogger(getClass());
	@Autowired
	private SignService service;

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@ApiOperation(value = "新增签到")
	@ResponseBody
	public String add(@ApiParam(required = true, value = "签到信息") @RequestBody Sign sign) throws Exception {
		service.add(sign);
		return "success";
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ApiOperation(value = "修改签到")
	@ResponseBody
	public String update(@ApiParam(required = true, value = "签到信息") @RequestBody Sign sign) throws Exception {
		service.update(sign);
		return "success";
	}

	@ApiOperation(value = "删除签到")
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public String del(@ApiParam(required = true, value = "id") @PathVariable Integer id) {
		service.delete(id);
		return "success";
	}
	
	@ApiOperation(value = "根据id获取签到")
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Sign getById(@ApiParam(required = true, value = "id") @PathVariable Integer id) {
		return service.getById(id);
	}

	@ApiOperation(value = "查找签到")
	@RequestMapping(value = "/find", method = RequestMethod.GET)
	public List<Sign> find(@ApiParam(required = true, value = "签到信息") @RequestBody Sign sign,
			@ApiParam(required = true, value = "已有") @RequestParam("has") Integer has,
			@ApiParam(required = true, value = "加载数量") @RequestParam("nextSize") Integer nextSize) {
		return service.query(sign, has, nextSize);
	}

}