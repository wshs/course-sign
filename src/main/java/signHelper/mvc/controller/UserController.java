package signHelper.mvc.controller;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import signHelper.modle.User;
import signHelper.service.UserService;

@Controller
@RequestMapping("/user")
public class UserController {

	private final Logger logger = LogManager.getLogger(getClass());
	@Autowired
	private UserService service;

	@RequestMapping(value = "/regist", method = RequestMethod.POST)
	@ApiOperation(value = "用户注册",notes="学生classId不能为空，老师InstitutionId不能为空，deviceNumber，name，number，password，role不能为空")
	@ResponseBody
	public String regist(@ApiParam(required = true, value = "用户信息") @RequestBody User user) throws Exception {
		service.add(user);
		return "success";
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ApiOperation(value = "更新用户")
	@ResponseBody
	public String update(@ApiParam(required = true, value = "用户信息") @RequestBody User user) throws Exception {
		service.update(user);
		return "success";
	}

	@ApiOperation(value = "删除用户")
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public String del(@ApiParam(required = true, value = "id") @PathVariable Integer id) {
		service.delete(id);
		return "success";
	}
	
	@ApiOperation(value = "根据id获取用户")
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public User getById(@ApiParam(required = true, value = "id") @PathVariable Integer id) {
		User ret = service.getById(id);
		if(ret!=null)
			ret.setPassword(null);
		return ret;
	}

	@ApiOperation(value = "查找用户")
	@RequestMapping(value = "/find", method = RequestMethod.GET)
	@ResponseBody
	public List<User> find(@ApiParam(required = true, value = "用户信息") @RequestBody User user,
			@ApiParam(required = true, value = "已有") @RequestParam("has") Integer has,
			@ApiParam(required = true, value = "加载数量") @RequestParam("nextSize") Integer nextSize) {
		List<User> ret=service.query(user, has, nextSize);
		ret.forEach(u->u.setPassword(null));
		return ret;
	}

}