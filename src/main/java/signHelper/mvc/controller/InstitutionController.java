package signHelper.mvc.controller;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import signHelper.modle.Institution;
import signHelper.service.InstitutionService;

@Controller
@RequestMapping("/institution")
public class InstitutionController {

	private final Logger logger = LogManager.getLogger(getClass());
	@Autowired
	private InstitutionService service;

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@ApiOperation(value = "新增学院")
	@ResponseBody
	public String add(@ApiParam(required = true, value = "学院信息") @RequestBody Institution institution) throws Exception {
		service.add(institution);
		return "success";
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ApiOperation(value = "修改学院")
	@ResponseBody
	public String update(@ApiParam(required = true, value = "学院信息") @RequestBody Institution institution) throws Exception {
		service.update(institution);
		return "success";
	}

	@ApiOperation(value = "删除学院")
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public String del(@ApiParam(required = true, value = "id") @PathVariable Integer id) {
		service.delete(id);
		return "success";
	}
	
	@ApiOperation(value = "根据id获取学院")
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Institution getById(@ApiParam(required = true, value = "id") @PathVariable Integer id) {
		return service.getById(id);
	}

	@ApiOperation(value = "查找学院")
	@RequestMapping(value = "/find", method = RequestMethod.GET)
	@ResponseBody
	public List<Institution> find(@ApiParam(required = true, value = "学院信息") @RequestBody Institution institution,
			@ApiParam(required = true, value = "已有") @RequestParam("has") Integer has,
			@ApiParam(required = true, value = "加载数量") @RequestParam("nextSize") Integer nextSize) {
		return service.query(institution, has, nextSize);
	}
	
	@ApiOperation(value = "根据学校id查找学院")
	@RequestMapping(value = "/findBySchoolId", method = RequestMethod.GET)
	@ResponseBody
	public List<Institution> findBySchoolId(@ApiParam(required = true, value = "学校id") @RequestParam Integer schoolId,
			@ApiParam(required = true, value = "已有") @RequestParam("has") Integer has,
			@ApiParam(required = true, value = "加载数量") @RequestParam("nextSize") Integer nextSize) {
		Institution institution=new Institution();
		institution.setSchoolId(schoolId);
		return service.query(institution, has, nextSize);
	}

}