package signHelper.mvc.controller;


import javax.validation.Valid;
import javax.validation.constraints.Size;
import javax.websocket.server.PathParam;

import org.hibernate.validator.constraints.Range;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import signHelper.modle.User;
import signHelper.service.UserService;

@Api("用于测试的控制器")
@Controller
@RequestMapping("/test")
public class TestController {

	private @Autowired UserService testService;  
	
	
	@ApiOperation(value = "根据用户名获取用户对象")
	@RequestMapping(value="",method = RequestMethod.POST)  
	@ResponseBody
    public User add(@ApiParam(required=true,name="test",value="测试")@Valid User test){  
        System.out.println(test); 
        return test;  
    }  
	
	@ApiOperation(value="根据用户名获取信息")
	@RequestMapping(value="/{id}",method = RequestMethod.GET)  
    public String getName(@ApiParam(required=true,value="测试")@PathVariable Integer id){  
		System.out.println(id); 

        return id.toString();  
    }  
	
	@ApiOperation(value="根据用户名获取信息")
	@RequestMapping(value="/param",method = RequestMethod.GET)  
    public String param(@ApiParam(required=true,value="测试")@RequestParam("id") Integer id){  
		System.out.println(id); 
        return id.toString();  
    }  
}
