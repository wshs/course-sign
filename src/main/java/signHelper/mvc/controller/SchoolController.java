package signHelper.mvc.controller;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import signHelper.modle.School;
import signHelper.service.SchoolService;

@Controller
@RequestMapping("/school")
public class SchoolController {

	private final Logger logger = LogManager.getLogger(getClass());
	@Autowired
	private SchoolService service;

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@ApiOperation(value = "新增学校")
	@ResponseBody
	public String add(@ApiParam(required = true, value = "学校信息") @RequestBody School school) throws Exception {
		service.add(school);
		return "success";
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ApiOperation(value = "修改学校")
	@ResponseBody
	public String update(@ApiParam(required = true, value = "学校信息") @RequestBody School school) throws Exception {
		service.update(school);
		return "success";
	}

	@ApiOperation(value = "删除学校")
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public String del(@ApiParam(required = true, value = "id") @PathVariable Integer id) {
		service.delete(id);
		return "success";
	}
	
	@ApiOperation(value = "根据id获取学校")
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public School getById(@ApiParam(required = true, value = "id") @PathVariable Integer id) {
		return service.getById(id);
	}

	@ApiOperation(value = "查找学校")
	@RequestMapping(value = "/find", method = RequestMethod.GET)
	@ResponseBody
	public List<School> find(@ApiParam(value = "学校信息") @RequestBody(required=false) School school,
			@ApiParam(required = true, value = "已有") @RequestParam("has") Integer has,
			@ApiParam(required = true, value = "加载数量") @RequestParam("nextSize") Integer nextSize) {
		if(school==null)
			school=new School();
		return service.query(school, has, nextSize);
	}

}