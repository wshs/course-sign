package signHelper.mvc;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 
 * @author <a href="mailto:1360527082@qq.com">王焕</a>
 * @since 0.1.0
 */
public class CenterExceptionUtil {

	public static ApiException createCenterException(String code) {
		return new ApiException(getExceptionCode(code), CodeUtil.getZH_Value(code));
	}

	public static ApiException createCenterException(String code, String remarkMsg) {
		return new ApiException(getExceptionCode(code), CodeUtil.getZH_Value(code) + "," + remarkMsg);
	}

	private static String getExceptionCode(String code) {
		String prefix = CodeUtil.getZH_Value("prefix");
		StringBuilder stbd = new StringBuilder(prefix);
		stbd.append(code);
		return stbd.toString();

	}

	static class CodeUtil {
		private static Properties ZH_DESC;

		/**
		 * 加载property文件
		 */
		static {
			try {
				ZH_DESC = getProperties(CodeUtil.class.getClassLoader()
						.getResourceAsStream("conf/return_code_zh_CN.properties"));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		/**
		 * 获取property文件输入流
		 *
		 * @param inputStream
		 *            输入流
		 * @return Property对象
		 * @throws IOException
		 */
		private static Properties getProperties(InputStream inputStream) throws IOException {
			Properties prop = new Properties();
			prop.load(inputStream);
			return prop;
		}

		/**
		 * 根据code获取中文描述
		 *
		 * @param code
		 *            错误码
		 * @return 错误消息
		 */
		public static String getZH_Value(String code) {
			return ZH_DESC.getProperty(code);
		}
	}
}
